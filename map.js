export function parseMap(imdat) {
  const map = {
    red: [0, 0, 0],
    green: [0, 0, 0],
    walls: []
  }

  function isWall(x, y) {
    const i = (y * imdat.width + x) * 4
    return imdat.data[i] == 255 && imdat.data[i+1] == 255 && imdat.data[i+2] == 255
  }

  function isRed(x, y) {
    const i = (y * imdat.width + x) * 4
    return imdat.data[i] == 250 && imdat.data[i+1] == 105 && imdat.data[i+2] == 130
  }

  function isGreen(x, y) {
    const i = (y * imdat.width + x) * 4
    return imdat.data[i] == 150 && imdat.data[i+1] == 240 && imdat.data[i+2] == 110
  }

  for (let y = 1; y < imdat.height-1; y++) {
    for (let x = 1; x < imdat.width-1; x++) {
      if (isWall(x, y))
        continue
      if (isRed(x, y)) {
        map.red = [ x, 0, y ]
        continue
      }

      if (isGreen(x, y)) {
        map.green = [ x, 0, y ]
        continue
      }

      if (isWall(x+1, y)) {
        map.walls.push({ x: x+.5, y: 0, z: y, r: 90 })
      }
      if (isWall(x-1, y)) {
        map.walls.push({ x: x-.5, y: 0, z: y, r: 90 })
      }
      if (isWall(x, y+1)) {
        map.walls.push({ x: x, y: 0, z: y+.5, r: 0 })
      }
      if (isWall(x, y-1)) {
        map.walls.push({ x: x, y: 0, z: y-.5, r: 0 })
      }
    }
  }
  return map
}
