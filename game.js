import { Input, loadPixels, planeBox, gl, m4, programInfo, interpolate, pickedWeapon, lerp, Raycast, normalized, interpolateAngles, networking } from './util.js'
import { parseMap } from './map.js'
import { assaultrifleModel, handgunModel, playerModel, shotgunModel, sniperModel } from './model.js'
import { NET_IDLE, NET_JOINED, NET_SPECTATING } from './net.js'

document.querySelector("#display").style.display = ''

const icepackShape = twgl.primitives.createCubeBufferInfo(gl, .5)
const snowShape = twgl.primitives.createCubeBufferInfo(gl, .1)
const planeShape = twgl.primitives.createPlaneBufferInfo(gl, 1, 2)
const floorShape = twgl.primitives.createPlaneBufferInfo(gl, 24, 24)

const input = new Input(document.querySelector('#display'))
const camera = m4.identity()
const view = m4.identity()
const world = m4.identity()
const viewProjection = m4.identity()
let projection

const playerRedTex = twgl.createTexture(gl, {
  minMag: gl.NEAREST,
  wrap: gl.CLAMP_TO_EDGE,
  src: [
    243, 139, 168, 255,
    235, 160, 172, 255,
    235, 160, 172, 255,
    243, 139, 168, 255,
  ],
})
const playerGreenTex = twgl.createTexture(gl, {
  minMag: gl.NEAREST,
  wrap: gl.CLAMP_TO_EDGE,
  src: [
    166, 227, 161, 255,
    148, 226, 213, 255,
    148, 226, 213, 255,
    166, 227, 161, 255,
  ],
})

const icepackTex = twgl.createTexture(gl, {
  minMag: gl.NEAREST,
  wrap: gl.CLAMP_TO_EDGE,
  src: [
    116, 199, 236, 255,
    137, 180, 250, 255,
    116, 199, 236, 255,

    137, 180, 250, 255,
    116, 199, 236, 255,
    137, 180, 250, 255,

    116, 199, 236, 255,
    137, 180, 250, 255,
    116, 199, 236, 255,
  ],
})

const demoTex = twgl.createTexture(gl, {
  minMag: gl.NEAREST,
  wrap: gl.CLAMP_TO_EDGE,
  src: [
    203, 166, 247, 255,
    203, 166, 247, 255,
    203, 166, 247, 255,

    108, 112, 135, 255,
    108, 112, 135, 255,
    108, 112, 135, 255,

    203, 166, 247, 255,
    203, 166, 247, 255,
    203, 166, 247, 255,
  ],
})

const wallTex = twgl.createTexture(gl, {
  minMag: gl.NEAREST,
  wrap: gl.CLAMP_TO_EDGE,
  src:[
    205, 214, 244, 255,
    205, 214, 244, 255,
    205, 214, 244, 255,
    205, 214, 244, 255,

    186, 194, 222, 255,
    186, 194, 222, 255,
    186, 194, 222, 255,
    186, 194, 222, 255,

    166, 173, 200, 255,
    166, 173, 200, 255,
    166, 173, 200, 255,
    166, 173, 200, 255,

    205, 214, 244, 255,
    205, 214, 244, 255,
    205, 214, 244, 255,
    205, 214, 244, 255,
  ],
})

const floorTex = twgl.createTexture(gl, {
  minMag: gl.BICUBIC,
  wrap: gl.CLAMP_TO_EDGE,
  src: [
    49, 50, 68, 255,
    75, 96, 84, 255,
    49, 50, 68, 255,
    75, 96, 84, 255,

    75, 96, 84, 255,
    49, 50, 68, 255,
    75, 96, 84, 255,
    49, 50, 68, 255,

    49, 50, 68, 255,
    75, 96, 84, 255,
    49, 50, 68, 255,
    75, 96, 84, 255,

    75, 96, 84, 255,
    49, 50, 68, 255,
    75, 96, 84, 255,
    49, 50, 68, 255,

  ],
})

const snowTex = twgl.createTexture(gl, {
  minMag: gl.NEAREST,
  wrap: gl.CLAMP_TO_EDGE,
  src: [
    255, 255, 255, 255,
  ],
})

const mapImg = await loadPixels('map.png')
const map = parseMap(mapImg)

const objects = [], overlayObjects = []
const drawObjects = [], overlayDrawObjects = []

function addObject(tex, shape, x=0, y=0, z=0, rx=0, ry=0, rz=0) {
  const uniforms = {
    u_diffuse: tex,
    u_worldViewProjection: m4.identity(),
  }
  drawObjects.push({
    programInfo: programInfo,
    bufferInfo: shape,
    uniforms: uniforms,
  })
  const obj = {
    translation: [x, y, z],
    rotation: [rx * Math.PI / 180, ry * Math.PI / 180, rz * Math.PI / 180],
    uniforms: uniforms,
  }
  objects.push(obj)
  return obj
}

function addSnowball(x, y, z, sx, sy, sz) {
  const uniforms = {
    u_diffuse: snowTex,
    u_worldViewProjection: m4.identity(),
  }
  drawObjects.push({
    programInfo: programInfo,
    bufferInfo: snowShape,
    uniforms: uniforms,
  })
  objects.push({
    translation: [sx, sy, sz],
    pos: [x, y, z],
    rotation: [0, 0, 0],
    snow: 1,
    uniforms: uniforms,
  })
}

function pushModel(mod) {
  drawObjects.push(...mod.d)
  objects.push(...mod.o)
}

function pushModelOverlay(mod) {
  overlayDrawObjects.push(...mod.d)
  overlayObjects.push(...mod.o)
}
function popModelOverlay(mod) {
  for (const d of mod.d) {
    const i = overlayDrawObjects.indexOf(d)
    if (i >= 0)
      overlayDrawObjects.splice(i, 1)
  }
  for (const o of mod.o) {
    const i = overlayObjects.indexOf(o)
    if (i >= 0)
      overlayObjects.splice(i, 1)
  }
}

for (const wall of map.walls) {
  addObject(wallTex, planeShape, wall.x, wall.y, wall.z, 90, wall.r, 0)
}
addObject(floorTex, floorShape, 11.5, -1, 11.5)

const weapons = [
  handgunModel,
  assaultrifleModel,
  shotgunModel,
  sniperModel,
]

const killmarker = {}
const gun = weapons[pickedWeapon](demoTex)
let myTeamGreen = false, greensAlive = 999, redsAlive = 999, teamsMap = {}, sriscores = []
let health = 100, icepacked = false, canplaceicepack = true
let reloading = true, reloadTime = gun.reload
let shootTime = 0, ammo = 0, magazine = 99
let aiming = false, fov = 60, spread = 1
let walkTime = 0

document.querySelector('#crosshair').style.backgroundImage = `url(${gun.crosshair})`

function setVisibleHUD(s) {
  document.querySelector('#health').hidden = !s
  document.querySelector('#ammo').hidden = !s
}

function updateHUD() {
  document.querySelector('#health > span').style.width = `${health.toFixed(2)}%`
  document.querySelector('#ammo').textContent = `${ammo}/${magazine}`
}

function resize() {
  twgl.resizeCanvasToDisplaySize(gl.canvas)
  gl.canvas.width = window.innerWidth
  gl.canvas.height = window.innerHeight
  gl.viewport(0, 0, gl.canvas.width, gl.canvas.height)
  projection = m4.perspective(fov * Math.PI / 180, gl.canvas.clientWidth / gl.canvas.clientHeight, 0.01, 100)
}
resize()
window.addEventListener('resize', resize)

const cam = {
  pos: [ 2, 0, 2 ],
  rot: [ 0, 225, 0 ],
  vel: [ 0, 0, 0 ],
  tcoord: '0,0',
  locked: false,
}

const collchunks = {}
for (const obj of objects) {
  if (obj.uniforms.u_diffuse != wallTex)
    continue

  const x = obj.translation[0]
  const z = obj.translation[2]

  for (let dz = -1; dz <= 1; dz++) {
    for (let dx = -1; dx <= 1; dx++) {
      const tcoord = `${Math.round((x+dx)/3)},${Math.round((z+dz)/3)}`
      if (!(tcoord in collchunks))
        collchunks[tcoord] = []

      if (collchunks[tcoord].indexOf(obj) == -1)
        collchunks[tcoord].push(obj)
    }
  }
}

function deleteIcePack() {
  const ip = referenced.find(x => x.ref && x.ref.icepack)
  if (!ip)
    return
  let i = referenced.indexOf(ip)
  if (i >= 0)
    referenced.splice(i, 1)

  i = objects.indexOf(ip)
  if (i >= 0) {
    objects.splice(i, 1)
    drawObjects.splice(i, 1)
  }
}

function collision() {
  const box = {
    translation: cam.pos,
    scale: [ .2, .2, .2 ]
  }
  if (cam.tcoord in collchunks)
    for (const obj of collchunks[cam.tcoord])
      if (planeBox(obj, box))
        return true
  return false
}

let moved = true, movlim = 0
function tryMove(x, z) {
  if (spread < 3)
    spread += .3

  walkTime = 0

  const pos = cam.pos.map(v => v)
  const gunpos = gun.o[0].translation.map(v => v)
  if (collision()) {
    cam.pos[0] += x
    cam.pos[2] += z
    gun.o[0].translation[0] += x
    gun.o[0].translation[2] += z
    moved = true
    return
  }

  cam.pos[0] += x
  gun.o[0].translation[0] += x
  if (collision()) {
    cam.pos[0] = pos[0]
    gun.o[0].translation[0] = gunpos[0]
  } else {
    moved = true
  }

  cam.pos[2] += z
  gun.o[0].translation[2] += z
  if (collision()) {
    cam.pos[2] = pos[2]
    gun.o[0].translation[2] = gunpos[2]
  } else {
    moved = true
  }

  cam.tcoord = `${Math.round(cam.pos[0]/3)},${Math.round(cam.pos[2]/3)}`
}

window.addEventListener('mousemove', ev => {
  if (cam.locked)
    return

  const x = ev.movementX
  const y = ev.movementY

  let sensitivity = aiming ? .05 : .1
  if (aiming && gun.scope)
    sensitivity = .025

  cam.rot[1] += x * -sensitivity
  cam.rot[0] += y * -sensitivity
  cam.rot[2] = lerp(cam.rot[2], x * .4, sensitivity)

  moved = true

  if (cam.rot[0] > 90)
    cam.rot[0] = 90
  if (cam.rot[0] < -90)
    cam.rot[0] = -90
  if (cam.rot[1] >= 360)
    cam.rot[1] -= 360
  if (cam.rot[1] <= -360)
    cam.rot[1] += 360
})
document.querySelector('#display').addEventListener('click', async ev => {
  if (!document.pointerLockElement) {
    await ev.target.requestPointerLock()
  }
})

const players = {}
const referenced = []

function cameraRay() {
  const hcalc = Math.cos(cam.rot[1] * Math.PI / 180)
  const vcalc = Math.sin(cam.rot[1] * Math.PI / 180)
  const ycalc = Math.sin(cam.rot[0] * Math.PI / 180)

  for (let i = 0; i < gun.rounds; i++) {
    const C = Math.PI / 180
    const dx = (Math.random() * 2 - 1) * spread * C
    const dy = (Math.random() * 2 - 1) * spread * C
    const dz = (Math.random() * 2 - 1) * spread * C

    const ray = new Raycast(
      cam.pos.map(v => v),
      normalized([ -vcalc+dx, ycalc+dy, -hcalc+dz ])
    )

    const point = ray.raycast(
      collchunks,
      referenced,
      (o, d) => o.ref || d >= 1
    )
    if (point) {
      if (point.obj.ref) {
        if (point.obj.ref.player) {
          const y = point.pos[1] - point.obj.ref.position[1]
          const dropoff = Math.max(point.dst / 30 - gun.range / 100, 0)
          const damage = (y > .15 ? gun.headshot : gun.damage) * (1-dropoff*2) / gun.rounds
          killmarker[point.obj.ref.cid] = Date.now()
          networking.message(`${networking.via}/hit`, {
            damage,
            target: point.obj.ref.cid
          })
        } else if (point.obj.ref.icepack && myTeamGreen) {
          icepacked = false
          networking.message(`${networking.via}/hiticepack`)
          deleteIcePack()
        }
      }
      addSnowball(...point.pos, ...cam.pos)
      networking.message(`${networking.via}/snowball`, { pos: point.pos, srcpos: cam.pos })
    }
  }
}

let lastTime = 0
function render(time) {
  time *= 0.001
  let delta = time - lastTime
  lastTime = time

  if (delta > 1) {
    console.warn('unusually high delta:', parseFloat(delta.toFixed(2)))
    delta = .1
  }

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

  let spd = aiming ? 1 : 2
  if (gun.scope && aiming)
    spd = .5
  const hcalc = Math.cos(cam.rot[1] * Math.PI / 180)
  const vcalc = Math.sin(cam.rot[1] * Math.PI / 180)
  const hmove = spd * delta * hcalc
  const vmove = spd * delta * vcalc

  if (moved && time - movlim >= 1/15) {
    networking.message(`${networking.via}/move`, {
      pos: cam.pos,
      head: cam.rot[1],
      pitch: cam.rot[0]
    })
    movlim = time
    moved = false
  }

  if (!cam.locked) {
    if (input.isKeyHeld('w'))
      tryMove(-vmove, -hmove)
    if (input.isKeyHeld('s'))
      tryMove(vmove, hmove)
    if (input.isKeyHeld('a'))
      tryMove(-hmove, vmove)
    if (input.isKeyHeld('d'))
      tryMove(hmove, -vmove)

    cam.pos[0] += cam.vel[0] * delta
    cam.pos[1] += cam.vel[1] * delta
    cam.pos[2] += cam.vel[2] * delta

    cam.vel[1] -= 9.81 * delta
    if (cam.pos[1] <= 0) {
      cam.pos[1] = 0
      cam.vel[1] = 0
    }

    if (Math.abs(cam.vel[1]) > .1)
      moved = true

    if (input.isKeyTapped(' ') && cam.pos[1] < .01 && !aiming) {
      cam.vel[1] = 3
      spread += 2
    }
  }

  if (reloading) {
    reloadTime -= delta
    if (reloadTime < 0) {
      reloading = false
      ammo = gun.ammo
      updateHUD()
    }
  }
  if (input.isKeyTapped('r') && magazine > 0) {
    magazine--
    updateHUD()
    reloading = true
    reloadTime = gun.reload
  }

  if (shootTime > 0)
    shootTime -= delta

  spread = lerp(spread, aiming ? gun.aim : gun.spread, 2 * delta)

  if ((gun.auto ? input.isMouseHeld(0) : input.isMouseTapped(0)) && !reloading && shootTime < .01) {
    if (ammo <= 0) {
      if (magazine > 0) {
        magazine--
        updateHUD()
        reloading = true
        reloadTime = gun.reload
      }
    } else {
      ammo--
      updateHUD()
      shootTime = gun.firerate
      cameraRay()

      gun.o[0].rotation[0] += -gun.firerate
      gun.o[0].translation[0] += (Math.random() * .2 - .1)
      gun.o[0].translation[2] += (Math.random() * .2 - .1)
      cam.rot[0] += gun.recoil

      if (gun.scope)
        input.mousekeys.delete(2)

      if (ammo <= 0 && magazine > 0) {
        magazine--
        updateHUD()
        reloading = true
        reloadTime = gun.reload
      }
    }
  }

  if (aiming && !input.isMouseHeld(2)) {
    fov = 60
    resize()
  } else if (!aiming && input.isMouseHeld(2)) {
    fov = gun.scope ? 10 : 50
    resize()
  }
  aiming = input.isMouseHeld(2)

  gun.o[0].rot[0] = -cam.rot[0] * Math.PI / 180
  gun.o[0].rot[1] = (cam.rot[1]+180) * Math.PI / 180 + (reloading ? .5 : 0)
  gun.o[0].rot[2] = reloading ? -.5 : 0
  gun.o[0].pos[0] = cam.pos[0]-(aiming ? 0 : hcalc*-.15)-vcalc*.3
  gun.o[0].pos[1] = cam.pos[1]-(aiming ? .05 : .1)+Math.sin(cam.rot[0] * Math.PI/180)*.3-(aiming & gun.scope ? .3 : 0)
  gun.o[0].pos[2] = cam.pos[2]+(aiming ? 0 : vcalc*-.15)-hcalc*.3

  interpolate(gun.o[0].pos, gun.o[0].translation, 20*delta)
  interpolateAngles(gun.o[0].rot, gun.o[0].rotation, 20*delta)

  if (walkTime < .1) {
    cam.rot[2] = lerp(cam.rot[2], Math.cos(time * 5 * spd) * spd * .5, .2)
  } else {
    cam.rot[2] = lerp(cam.rot[2], 0, .2)
  }
  walkTime += delta

  if (networking.state == NET_JOINED && input.isKeyTapped('i') && canplaceicepack && !myTeamGreen) {
    console.log('placing icepack')
    canplaceicepack = false
    icepacked = true
    networking.timer = 60
    const obj = addObject(icepackTex, icepackShape, ...cam.pos)
    obj.scale = [ .5, .5, .5 ]
    obj.ref = { icepack: true }
    referenced.push(obj)
    networking.message(`${networking.via}/icepack`, { pos: cam.pos })
  }

  m4.identity(camera)
  m4.translate(camera, cam.pos, camera)
  m4.rotateY(camera, cam.rot[1] * Math.PI/180, camera)
  m4.rotateX(camera, cam.rot[0] * Math.PI/180, camera)
  m4.rotateZ(camera, cam.rot[2] * Math.PI/180, camera)
  m4.inverse(camera, view)
  m4.multiply(projection, view, viewProjection)

  for (const cid in players)
    players[cid].update(delta)

  let del = []
  for (const obj of objects) {
    const uni = obj.uniforms
    m4.identity(world)
    m4.translate(world, obj.translation, world)
    m4.rotateY(world, obj.rotation[1], world)
    m4.rotateX(world, obj.rotation[0], world)
    m4.rotateZ(world, obj.rotation[2], world)

    if (obj.snow) {
      m4.scale(world, [obj.snow, obj.snow, obj.snow], world)
      obj.translation[0] = lerp(obj.translation[0], obj.pos[0], 10 * delta)
      obj.translation[1] = lerp(obj.translation[1], obj.pos[1], 10 * delta)
      obj.translation[2] = lerp(obj.translation[2], obj.pos[2], 10 * delta)
      obj.snow -= delta
      if (obj.snow < .01)
        del.push(obj)
    }

    if (obj.ref && obj.translation[1] > 5)
      m4.scale(world, [0, 0, 0], world)

    m4.multiply(viewProjection, world, uni.u_worldViewProjection)
  }
  twgl.drawObjectList(gl, drawObjects)
  for (const o of del) {
    const i = objects.indexOf(o)
    if (i >= 0) {
      objects.splice(i, 1)
      drawObjects.splice(i, 1)
    }
  }

  gl.clear(gl.DEPTH_BUFFER_BIT)
  for (const obj of overlayObjects) {
    const uni = obj.uniforms
    m4.identity(world)
    m4.translate(world, obj.translation, world)
    m4.rotateY(world, obj.rotation[1], world)
    m4.rotateX(world, obj.rotation[0], world)
    m4.rotateZ(world, obj.rotation[2], world)
    m4.multiply(viewProjection, world, uni.u_worldViewProjection)
  }
  twgl.drawObjectList(gl, overlayDrawObjects)

  requestAnimationFrame(render)
}

function checkMatchState() {
  if (greensAlive > 0 && redsAlive > 0)
    return true

  const scores = networking.scores.map(v => v)
  if (redsAlive <= 0)
    scores[1]++
  if (greensAlive <= 0)
    scores[0]++

  networking.sync(1, undefined, scores)
}

networking.newRef = (cid, weapon) => {
  const player = playerModel(weapons[weapon](demoTex), playerRedTex, 12, 20, 12)
  player.cid = cid
  pushModel(player)
  players[cid] = player
  referenced.push(...player.o)
  return player
}
networking.delRef = cid => {
  for (const o of players[cid].o) {
    let i = objects.indexOf(o)
    if (i >= 0)
      objects.splice(i, 1)

    i = referenced.indexOf(o)
    if (i >= 0)
      referenced.splice(i, 1)
  }
  for (const d of players[cid].d) {
    const i = drawObjects.indexOf(d)
    if (i >= 0)
      drawObjects.splice(i, 1)
  }

  delete players[cid]
}
networking.timerHit = () => {
  if (networking.state == NET_IDLE) {
    if (Object.keys(players).length > 0) {
      networking.sync(181, NET_JOINED)
      console.log('switched to JOINED')
    } else {
      networking.sync(30, NET_IDLE)
      console.log('kept on IDLE')
    }
  } else {
    networking.sync(30, NET_IDLE)
    console.log('switched to IDLE')
  }
}
networking.stateChange = state => {
  console.log('new state:', state)

  if (state == NET_IDLE && icepacked) {
    if (redsAlive > 0 && greensAlive > 0) {
      sriscores[0]++
      networking.sync(undefined, undefined, sriscores)
    }
  }

  deleteIcePack()
  canplaceicepack = true

  if (state == NET_JOINED) {
    console.log('picking team')
    sriscores = networking.scores.map(v => v)
    teamsMap = {}
    greensAlive = 0
    redsAlive = 0
    myTeamGreen = false
    icepacked = false
    networking.pickTeam().then(teams => {
      document.querySelector('#timer').style.borderColor = '#faa'
      let i = 0
      let spawn = map.red
      for (const u of teams) {
        const green = i++ >= (teams.length / 2)
        teamsMap[u.$cid] = green
        if (green)
          greensAlive++
        else
          redsAlive++

        if (u.$cid in players)
          for (const o of players[u.$cid].o) {
            o.uniforms.u_diffuse = green ? playerGreenTex : playerRedTex
          }
        if (networking.cid == u.$cid && green) {
          document.querySelector('#timer').style.borderColor = '#afa'
          myTeamGreen = true
          spawn = map.green
        }
      }

      console.log('team picked!')
      health = 100
      reloadTime = gun.reload
      shootTime = 0
      ammo = 0
      reloading = true
      magazine = 99
      pushModelOverlay(gun)
      cam.pos[0] = spawn[0]
      cam.pos[1] = 0
      cam.pos[2] = spawn[2]
      moved = true
      cam.locked = false
      updateHUD()
      setVisibleHUD(true)
    })
  } else {
    setVisibleHUD(false)
    cam.locked = false
    reloadTime = 9999
    shootTime = 9999
    popModelOverlay(gun)

    cam.pos[0] = 11.5 + Math.random() * 2 - 1
    cam.pos[1] = 0
    cam.pos[2] = 11.5 + Math.random() * 2 - 1
    moved = true
    if (state == NET_SPECTATING) {
      cam.locked = true
      cam.pos[1] = 20
      cam.rot[0] = -90
      cam.rot[1] = 0
    } else {
      icepacked = false
    }
  }
}
networking.onTimer = () => {
  const el = document.querySelector('#timer')
  const text = `${Math.floor(networking.timer / 60)}:${(networking.timer % 60).toFixed(0).padStart(2, '0')}`

  if (networking.state == NET_IDLE && referenced.length == 0) {
    const fun = '....'
    const i = 3 - (networking.timer % 4)
    el.textContent = fun.substring(0, i) + '|' + fun.substring(i+1)
  } else {
    el.textContent = text
  }

  if (networking.state == NET_JOINED) {
    el.style.color = '#f38ba8'
  } else {
    el.style.color = '#f9e2af'
  }
  if (icepacked)
    el.style.color = '#89b4fa'

  if (networking.state == NET_SPECTATING) {
    let ib = true
    for (const cid in players)
      if (players[cid].position[1] < 5)
        ib = false

    if (ib)
      networking.sync(30, NET_IDLE)
  }
}

networking.onMessage(`${networking.via}/snowball`, msg => {
  addSnowball(...msg.pos, ...msg.srcpos)
})
networking.onMessage(`${networking.via}/hit`, msg => {
  if (networking.state != NET_JOINED)
    return

  delete killmarker[msg.target]

  console.log('damage', msg.damage, msg.target)
  if (msg.target == networking.cid && health > 0 && networking.state == NET_JOINED) {
    if (msg.$cid in teamsMap && teamsMap[msg.$cid] == myTeamGreen)
      return

    health -= msg.damage
    if (health <= 0) {
      localStorage.setItem('@flanker/kval', parseInt(localStorage.getItem('@flanker/kval')) - 1)
      console.log('you died loser', myTeamGreen ? '(green)' : '(red)')
      health = 0

      if (myTeamGreen)
        greensAlive--
      else
        redsAlive--
      console.log('red/green', redsAlive, greensAlive)
      networking.message(`${networking.via}/died`)

      if (checkMatchState()) {
        networking.state = NET_SPECTATING
        networking.stateChange(networking.state)
      }
    }
    updateHUD()
  }
})
networking.onMessage(`${networking.via}/icepack`, msg => {
  networking.timer = 60
  icepacked = true
  canplaceicepack = false
  const obj = addObject(icepackTex, icepackShape, ...msg.pos)
  obj.scale = [ .5, .5, .5 ]
  obj.ref = { icepack: true }
  referenced.push(obj)
})
networking.onMessage(`${networking.via}/hiticepack`, _ => {
  icepacked = false
  deleteIcePack()
})
networking.onMessage(`${networking.via}/died`, msg => {
  if (msg.$cid in killmarker && killmarker[msg.$cid] - Date.now() < 3000) {
    localStorage.setItem('@flanker/kval', parseInt(localStorage.getItem('@flanker/kval')) + 1)
  }

  if (msg.$cid in teamsMap) {
    if (teamsMap[msg.$cid])
      greensAlive--
    else
      redsAlive--
    console.log('red/green', redsAlive, greensAlive)
  }
})
networking.scoresChange = scores => {
  console.info("new scores (red/green):", scores)
  document.querySelector('#redscore').textContent = scores[0].toString().padStart(2, '0')
  document.querySelector('#greenscore').textContent = scores[1].toString().padStart(2, '0')

  document.querySelector('#redscore').style.borderColor = scores[0] > scores[1] ? '#faa' : ''
  document.querySelector('#greenscore').style.borderColor = scores[1] > scores[0] ? '#afa' : ''
}

await networking.join(pickedWeapon)
document.querySelector('#top').hidden = false
if (networking.state == NET_IDLE || networking.state == NET_SPECTATING)
  networking.stateChange(networking.state)
requestAnimationFrame(render)
