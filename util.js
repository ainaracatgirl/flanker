twgl.setDefaults({ attribPrefix: 'a_' })
const m4 = twgl.m4

const gl = document.querySelector("#display").getContext("webgl")
gl.enable(gl.DEPTH_TEST)
//gl.enable(gl.CULL_FACE)
const programInfo = twgl.createProgramInfo(gl, ["vs", "fs"])

export { m4, gl, programInfo }

export let pickedWeapon = 0
export function pickWeapon(i) {
  pickedWeapon = i
}

export let networking = null
export function setNetworking(n) {
  networking = n
}

export async function loadPixels(url) {
  return new Promise(res => {
    const img = new Image()
    img.onload = () => {
      const cnv = document.createElement('canvas')
      cnv.width = img.width
      cnv.height = img.height
      const ctx = cnv.getContext('2d')
      ctx.drawImage(img, 0, 0)
      const imdat = ctx.getImageData(0, 0, img.width, img.height)
      cnv.remove()
      img.remove()
      res(imdat)
    }
    img.src = url
  })
}

export function planeBox(plane, box) {
  const EPSILON = .01
  const other = {
    translation: plane.translation,
    scale: [ plane.rotation[1] != 0 ? EPSILON : .5, 1, plane.rotation[1] != 0 ? .5 : EPSILON ]
  }

  return boxBox(other, box)
}
export function boxBox(other, box) {
  const boxMinX = box.translation[0] - box.scale[0]
  const boxMaxX = box.translation[0] + box.scale[0]
  const boxMinY = box.translation[1] - box.scale[1]
  const boxMaxY = box.translation[1] + box.scale[1]
  const boxMinZ = box.translation[2] - box.scale[2]
  const boxMaxZ = box.translation[2] + box.scale[2]

  const otherMinX = other.translation[0] - other.scale[0]
  const otherMaxX = other.translation[0] + other.scale[0]
  const otherMinY = other.translation[1] - other.scale[1]
  const otherMaxY = other.translation[1] + other.scale[1]
  const otherMinZ = other.translation[2] - other.scale[2]
  const otherMaxZ = other.translation[2] + other.scale[2]

  const intersectionX =
    otherMaxX >= boxMinX && otherMinX <= boxMaxX
  const intersectionY =
    otherMaxY >= boxMinY && otherMinY <= boxMaxY
  const intersectionZ =
    otherMaxZ >= boxMinZ && otherMinZ <= boxMaxZ

  return intersectionX && intersectionY && intersectionZ
}

export function lerp(a, b, k) {
  return a * (1-k) + b * k
}

// https://gist.github.com/shaunlebron/8832585
export function lerpAngle(a, b, k) {
  const max = Math.PI * 2
  const da = (b - a) % max
  const dist = 2 * da % max - da
  return a + dist * k
}

export function interpolate(target, source, k) {
  source[0] = lerp(source[0], target[0], k)
  source[1] = lerp(source[1], target[1], k)
  source[2] = lerp(source[2], target[2], k)
}

export function interpolateAngles(target, source, k) {
  source[0] = lerpAngle(source[0], target[0], k)
  source[1] = lerpAngle(source[1], target[1], k)
  source[2] = lerpAngle(source[2], target[2], k)
}

export function normalized(v) {
  const l = Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2])

  return [ v[0] / l, v[1] / l, v[2] / l ]
}
export function divsmall(v) {
  const s = Math.max(...v)
  return v.map(x => x / s)
}

export class Raycast {
  pos = [0,0,0]
  dir = [0,0,0]
  step = .01
  maxsteps = 3000

  constructor(pos, dir) {
    this.pos = pos
    this.dir = dir
  }

  raycast(chunks, nonchunks, valid=()=>true) {
    const box = {
      translation: this.pos,
      scale: [ this.step, this.step, this.step ]
    }

    for (let i = 0; i < this.maxsteps; i++) {
      const tcoord = `${Math.round(box.translation[0]/3)},${Math.round(box.translation[2]/3)}`

      if (tcoord in chunks)
        for (const obj of chunks[tcoord])
          if (planeBox(obj, box))
            if (valid(obj, i*this.step))
              return { pos: this.pos, dst: i * this.step, obj }

      for (const obj of nonchunks)
        if (boxBox(obj, box))
          if (valid(obj, i*this.step))
            return { pos: this.pos, dst: i * this.step, obj }

      box.translation[0] += this.dir[0] * this.step
      box.translation[1] += this.dir[1] * this.step
      box.translation[2] += this.dir[2] * this.step
    }
  }
}

export class Input {
  constructor(elem) {
    this.elem = elem
    this.keys = new Set()
    this.mousekeys = new Set()
    this.mouseX = 0
    this.mouseY = 0

    this.keydown = window.addEventListener('keydown', ev => {
      if (ev.repeat) return

      this.keys.add(ev.key.toLowerCase())
    })
    this.keyup = window.addEventListener('keyup', ev => {
      this.keys.delete(ev.key.toLowerCase())
    })
    this.mousemove = window.addEventListener('mousemove', ev => {
      this.mouseX = ev.clientX / this.elem.clientWidth * this.elem.width
      this.mouseY = ev.clientY / this.elem.clientHeight * this.elem.height
    })
    this.mousedown = window.addEventListener('mousedown', ev => {
      this.mousekeys.add(ev.button)
    })
    this.mouseup = window.addEventListener('mouseup', ev => {
      this.mousekeys.delete(ev.button)
    })
  }

  isKeyHeld(key) {
    return this.keys.has(key.toLowerCase())
  }

  isKeyTapped(key) {
    const v = this.keys.has(key.toLowerCase())
    if (v) this.keys.delete(key.toLowerCase())
    return v
  }

  isMouseHeld(key) {
    return this.mousekeys.has(key)
  }

  isMouseTapped(key) {
    const v = this.mousekeys.has(key)
    if (v) this.mousekeys.delete(key)
    return v
  }
}
