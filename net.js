export const NET_DISCONNECTED = 0
export const NET_CONNECTED = 1
export const NET_JOINED = 2
export const NET_SPECTATING = 3
export const NET_IDLE = 4

export class NetworkingBase {
  state = NET_DISCONNECTED
  timer = 30
  via = '@flanker/global'
  cid = crypto.randomUUID()
  scores = [ 0, 0 ]
  weapon = 0
  weapons = {}
  clients = {}
  teampicker = []

  constructor(via) {
    this.via = `@flanker/${via}`
  }

  newRef(_) { throw 'not implemented' }
  delRef(_) { throw 'not implemented' }
  timerHit() { throw 'not implemented' }
  stateChange(_) { console.warn('not implemented') }
  scoresChange(_) { console.warn('not implemented') }

  onMessage(_, __=()=>{}) {}
  message(_, __={}) {}
  async waitMessage(_, __=1000) {}
  async getMessages(_, __=1000) {}

  sync(time, state, scores) {
    if (time)
      this.timer = time
    if (state) {
      if (state != this.state)
        this.stateChange(state)
      this.state = state
    }
    if (scores) {
      this.scoresChange(scores)
      this.scores = scores
    }
    this.message(`${this.via}/sync`, { timer: this.timer, state, scores })
  }

  async pickTeam() {
    return new Promise(res => {
      if (!this.__randomteamn)
        this.__randomteamn = Math.random()

      const pick = { v: this.__randomteamn, $cid: this.cid }
      this.teampicker.push(pick)
      this.message(`${this.via}/pick`, pick)

      setTimeout(() => {
        this.teampicker.sort((a, b) => a.v - b.v)
        const teams = this.teampicker.map(v => v)
        this.teampicker = []
        res(teams)
      }, 1000)
    })
  }

  async join(weapon) {
    this.weapon = weapon
    this.state = NET_CONNECTED
    this.onMessage(`${this.via}/join`, msg => {
      if (msg.$cid in this.clients)
        return

      this.weapons[msg.$cid] = msg.weapon
      this.clients[msg.$cid] = {
        ref: this.newRef(msg.$cid, msg.weapon),
        t: Date.now(),
      }

      console.log(`join: ${msg.$cid}`)
      this.message(`${this.via}/hello`, { state: this.state, weapon: this.weapon })
      this.message(`${this.via}/sync`, { timer: this.timer, scores: this.scores })
    })
    this.onMessage(`${this.via}/hello`, msg => {
      if (!(msg.$cid in this.clients)) {
        this.weapons[msg.$cid] = msg.weapon
        this.clients[msg.$cid] = {
          ref: this.newRef(msg.$cid, msg.weapon),
          t: Date.now(),
        }
        console.log(`join: ${msg.$cid}`)
      }
    })
    this.onMessage(`${this.via}/heartbeat`, msg => {
      this.clients[msg.$cid].t = Date.now()
    })
    this.onMessage(`${this.via}/sync`, msg => {
      if (msg.state) {
        if (msg.state != this.state)
          this.stateChange(msg.state)
        this.state = msg.state
      }
      if (msg.scores) {
        this.scoresChange(msg.scores)
        this.scores = msg.scores
      }
      this.timer = msg.timer
      console.log('timer is at', this.timer)
    })
    this.onMessage(`${this.via}/move`, msg => {
      if (!this.clients[msg.$cid]) {
        if (msg.$cid in this.weapons) {
          this.clients[msg.$cid] = {
            ref: this.newRef(msg.$cid, this.weapons[msg.$cid]),
            t: Date.now(),
          }
          console.log(`rejoin: ${msg.$cid}`)
        } else {
          return
        }
      }

      this.clients[msg.$cid].ref.position[0] = msg.pos[0]
      this.clients[msg.$cid].ref.position[1] = msg.pos[1] - .25
      this.clients[msg.$cid].ref.position[2] = msg.pos[2]
      this.clients[msg.$cid].ref.heading = msg.head * Math.PI/180
      this.clients[msg.$cid].ref.pitch = msg.pitch * Math.PI/180
      this.clients[msg.$cid].ref.walking = .1
    })
    this.onMessage(`${this.via}/pick`, msg => {
      this.teampicker.push(msg)
    })

    this.message(`${this.via}/join`, { weapon })
    try {
      const { state } = await this.waitMessage(`${this.via}/hello`)
      if (state == NET_IDLE) {
        this.state = NET_IDLE
        this.timer = 30
      } else {
        this.state = NET_SPECTATING
      }
      console.log('joined on state:', this.state)
    } catch {
      this.timer = 30
      this.state = NET_IDLE
      console.log('first joined on state:', this.state)
    }

    setInterval(() => {
      this.timer--
      this.onTimer?.()
      if (this.timer < 1) {
        this.timerHit()
      }
    }, 1000)

    setInterval(() => {
      this.message(`${this.via}/heartbeat`)
    }, 1000)
    setInterval(() => {
      const del = []
      for (const c in this.clients)
        if (Date.now() - this.clients[c].t > 2500)
          del.push(c)
      for (const d of del) {
        delete this.clients[d]
        this.delRef(d)
        console.log(`left: ${d}`)
      }
    }, 2000)
  }
}

export class MockNetworking extends NetworkingBase {
  chn = new BroadcastChannel('flanker')

  static async matchmaking() {
    return new MockNetworking('global')
  }

  onMessage(type, fn=()=>{}) {
    this.chn.addEventListener('message', ev => {
      const pkt = JSON.parse(ev.data)
      if (pkt.type == type)
        fn({ ...pkt.payload, $cid: pkt.cid })
    })
  }

  message(type, obj={}) {
    this.chn.postMessage(JSON.stringify({
      type,
      payload: obj,
      cid: this.cid
    }))
  }

  async waitMessage(type, timeout=1000) {
    return new Promise((res, rej) => {
      const t = setTimeout(() => {
        this.chn.removeEventListener('message', fn)
        rej()
      }, timeout)
      const fn = ev => {
        const pkt = JSON.parse(ev.data)
        if (pkt.type == type) {
          this.chn.removeEventListener('message', fn)
          clearTimeout(t)
          res({ ...pkt.payload, $cid: pkt.cid })
        }
      }
      this.chn.addEventListener('message', fn)
    })
  }

  async getMessages(type, timeout=1000) {
    const list = []
    return new Promise(res => {
      setTimeout(() => {
        this.chn.removeEventListener('message', fn)
        res(list)
      }, timeout)
      const fn = ev => {
        const pkt = JSON.parse(ev.data)
        if (pkt.type == type) {
          list.push({ ...pkt.payload, $cid: pkt.cid })
        }
      }
      this.chn.addEventListener('message', fn)
    })
  }
}

export class WebSocketNetworking extends NetworkingBase {
  static async matchmaking() {
    const ws = new WebSocket(`wss://relay.jdevstudios.es/`)
    let n
    return new Promise(res => {
      ws.addEventListener('open', () => {
        ws.send(JSON.stringify({
          clientID: crypto.randomUUID(),
          listens: [
            `@flanker/instance`,
            `@flanker/discovery`,
          ]
        }))

        ws.addEventListener('message', async ev => {
          const pkt = JSON.parse(ev.data)
          if (pkt && pkt.event && pkt.event == `@flanker/discovery` && n && Object.keys(n.clients).length < 10) {
            ws.send(JSON.stringify({
              event: '@flanker/instance',
              $target: pkt.$sender,
              id: n.via.split('/').pop()
            }))
          } else if (pkt && pkt.event && pkt.event == `@flanker/instance` && !n) {
            n = await WebSocketNetworking.match(pkt.id)
            res(n)
          }
        })

        ws.send(JSON.stringify({
          event: `@flanker/discovery`
        }))

        setTimeout(async () => {
          if (!n) {
            n = await WebSocketNetworking.match(crypto.randomUUID())
            res(n)
          }
        }, 1000 + 2000 * Math.random())
      })
    })
  }

  static async match(id) {
    const n = new WebSocketNetworking(id)
    n.chn = new WebSocket(`wss://relay.jdevstudios.es/`)

    return new Promise(res => {
      n.chn.addEventListener('open', () => {
        n.chn.send(JSON.stringify({
          clientID: n.cid,
          listens: [
            `${n.via}/hello`,
            `${n.via}/join`,
            `${n.via}/move`,
            `${n.via}/hit`,
            `${n.via}/snowball`,
            `${n.via}/heartbeat`,
            `${n.via}/pick`,
            `${n.via}/sync`,
            `${n.via}/icepack`,
            `${n.via}/hiticepack`,
            `${n.via}/died`,
          ]
        }))
        res(n)
      })
    })
  }

  onMessage(event, fn=()=>{}) {
    this.chn.addEventListener('message', ev => {
      const pkt = JSON.parse(ev.data)
      if (pkt.event == event)
        fn({ ...pkt.payload, $cid: pkt.$sender })
    })
  }

  message(event, obj={}) {
    this.chn.send(JSON.stringify({
      event,
      payload: obj,
    }))
  }

  async waitMessage(event, timeout=1000) {
    return new Promise((res, rej) => {
      const t = setTimeout(() => {
        this.chn.removeEventListener('message', fn)
        rej()
      }, timeout)
      const fn = ev => {
        const pkt = JSON.parse(ev.data)
        if (pkt.event == event) {
          this.chn.removeEventListener('message', fn)
          clearTimeout(t)
          res({ ...pkt.payload, $cid: pkt.$sender })
        }
      }
      this.chn.addEventListener('message', fn)
    })
  }

  async getMessages(event, timeout=1000) {
    const list = []
    return new Promise(res => {
      setTimeout(() => {
        this.chn.removeEventListener('message', fn)
        res(list)
      }, timeout)
      const fn = ev => {
        const pkt = JSON.parse(ev.data)
        if (pkt.event == event) {
          list.push({ ...pkt.payload, $cid: pkt.$sender })
        }
      }
      this.chn.addEventListener('message', fn)
    })
  }
}
