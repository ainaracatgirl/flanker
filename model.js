import { gl, programInfo, m4, lerpAngle, lerp } from './util.js'

function createCube(x, y, z, io=0) {
  const sx = x / 2
  const sy = y / 2
  const sz = z / 2

  const vertices = new Float32Array([
    // Front face
    -sx, -sy, sz,
    sx, -sy, sz,
    sx, sy, sz,
    -sx, sy, sz,

    // Back face
    -sx, -sy, -sz,
    -sx, sy, -sz,
    sx, sy, -sz,
    sx, -sy, -sz,

    // Left face
    -sx, -sy, -sz,
    -sx, -sy, sz,
    -sx, sy, sz,
    -sx, sy, -sz,

    // Right face
    sx, -sy, -sz,
    sx, sy, -sz,
    sx, sy, sz,
    sx, -sy, sz,

    // Bottom face
    -sx, -sy, -sz,
    sx, -sy, -sz,
    sx, -sy, sz,
    -sx, -sy, sz,

    // Top face
    -sx, sy, -sz,
    -sx, sy, sz,
    sx, sy, sz,
    sx, sy, -sz,
  ])

  const texCoords = [
    0, 0, 1, 0, 1, 1, 0, 1, // Front face
    0, 0, 1, 0, 1, 1, 0, 1, // Back face
    0, 0, 1, 0, 1, 1, 0, 1, // Left face
    0, 0, 1, 0, 1, 1, 0, 1, // Right face 
    0, 0, 1, 0, 1, 1, 0, 1, // Bottom face
    0, 0, 1, 0, 1, 1, 0, 1  // Top face
  ]

  const normals = new Float32Array([
    // Front face
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,

    // Back face
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,

    // Left face
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,

    // Right face
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,

    // Bottom face
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,

    // Top face
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
  ])

  const indices = [
    0, 1, 2, 0, 2, 3,    // Front face
    4, 5, 6, 4, 6, 7,    // Back face
    8, 9, 10, 8, 10, 11, // Left face
    12, 13, 14, 12, 14, 15, // Right face
    16, 17, 18, 16, 18, 19, // Bottom face
    20, 21, 22, 20, 22, 23  // Top face
  ].map(v => v+io)

  return { vertices, texCoords, indices, normals }
}

function createCubeBufferInfo(cube) { 
  return twgl.createBufferInfoFromArrays(gl, {
    position: { numComponents: 3, data: cube.vertices },
    texcoord: { numComponents: 2, data: cube.texCoords },
    normal: { numComponents: 3, data: cube.normals },
    indices: cube.indices
  })
}

const transformVerticesMat = m4.identity()
function transformVertices(vertices, x=0, y=0, z=0, rx=0, ry=0, rz=0) {
  const mat = transformVerticesMat
  m4.identity(mat)
  m4.translate(mat, [x,y,z], mat)
  m4.rotateZ(mat, rz, mat)
  m4.rotateY(mat, ry, mat)
  m4.rotateX(mat, rx, mat)

  for (let i = 0; i < vertices.length; i += 3) {
    const vertex = [vertices[i], vertices[i + 1], vertices[i + 2]]

    m4.transformPoint(mat, vertex, vertex)

    vertices[i] = vertex[0]
    vertices[i + 1] = vertex[1]
    vertices[i + 2] = vertex[2]
  }
}
function transformCube(cube, x=0, y=0, z=0, rx=0, ry=0, rz=0) {
  transformVertices(cube.vertices, x, y, z, rx, ry, rz)
  transformVertices(cube.normals, x, y, z, rx, ry, rz)
}

function mixIntoModel(parts, tex, x, y, z) {
  const mixed = {
    vertices: parts.reduce((a, v) => [...v.vertices, ...a], []),
    texCoords: parts.reduce((a, v) => [...v.texCoords, ...a], []),
    indices: parts.reduce((a, v) => [...v.indices, ...a], []),
    normals: parts.reduce((a, v) => [...v.normals, ...a], []),
  }

  const shape = createCubeBufferInfo(mixed)
  const uniforms = {
    u_diffuse: tex,
    u_worldViewProjection: m4.identity(),
  }
  const drawObj = {
    programInfo: programInfo,
    bufferInfo: shape,
    uniforms: uniforms,
  }
  const obj = {
    translation: [x, y, z],
    rotation: [0, 0, 0],
    pos: [x, y, z],
    rot: [0, 0, 0],
    uniforms: uniforms,
  }
  return { d: [drawObj], o: [obj] }
}

export function handgunModel(tex, x=0, y=0, z=0) {
  const handle = createCube(.05/2, .1/2, .075/2)
  transformCube(handle, 0, -.1/2, .1/2, 20*Math.PI/180, 0, 0)

  const barrel = createCube(.1/2, .1/2, .3/2, handle.vertices.length/3)
  transformCube(barrel, 0, 0, .15/2)

  const aim = createCube(.05/2, .05/2, .05/2, handle.vertices.length/3 + barrel.vertices.length/3)
  transformCube(aim, 0, .045/2, .25/2, -15*Math.PI/180, 0, 0)

  return {
    ...mixIntoModel([ handle, barrel, aim ], tex, x, y, z),
    reload: 1,
    ammo: 5,
    firerate: .5,
    auto: false,
    scope: false,
    crosshair: 'crosshair.png',
    spread: 2,
    aim: 1,
    rounds: 1,
    recoil: 2.5,
    damage: 40, headshot: 80, range: 30,
  }
}

export function shotgunModel(tex, x=0, y=0, z=0) {
  const handle = createCube(.075/2, .2/2, -.1/2)
  transformCube(handle, 0, -.1/2, -.025/2, 30*Math.PI/180, 0, 0)

  const barrel = createCube(.1/2, .075/2, .75/2, handle.vertices.length/3)
  transformCube(barrel, 0, 0, .25/2)

  const aim = createCube(.08/2, .05/2, .05/2, handle.vertices.length/3 + barrel.vertices.length/3)
  transformCube(aim, 0, .045/2, .5/2, -15*Math.PI/180, 0, 0)

  const reload = createCube(.15/2, .1/2, .5/2, handle.vertices.length/3 + barrel.vertices.length/3 + aim.vertices.length/3)
  transformCube(reload, 0, -.03/2, .4/2)

  return {
    ...mixIntoModel([ handle, barrel, aim, reload ], tex, x, y, z),
    reload: 1.5,
    ammo: 1,
    firerate: 1,
    auto: false,
    scope: false,
    crosshair: 'crosshair-shotgun.png',
    spread: 4,
    aim: 2,
    rounds: 6,
    recoil: 5,
    damage: 90, headshot: 180, range: 20,
  }
}

export function sniperModel(tex, x=0, y=0, z=0) {
  const handle = createCube(.075/2, .2/2, -.1/2)
  transformCube(handle, 0, -.1/2, -.025/2, 30*Math.PI/180, 0, 0)

  const barrel = createCube(.1/2, .075/2, .75/2, handle.vertices.length/3)
  transformCube(barrel, 0, 0, .25/2)

  const deco = createCube(.05/2, .05/2, .05/2, handle.vertices.length/3 + barrel.vertices.length/3)
  transformCube(deco, 0, .06/2, .2/2)

  const scope = createCube(.075/2, .075/2, .5/2, handle.vertices.length/3 + barrel.vertices.length/3 + deco.vertices.length/3)
  transformCube(scope, 0, .1/2, .2/2)

  return {
    ...mixIntoModel([ handle, barrel, deco, scope ], tex, x, y, z),
    reload: 2,
    ammo: 1,
    firerate: 1,
    auto: false,
    scope: true,
    crosshair: 'crosshair-sniper.png',
    spread: 4,
    aim: 0,
    rounds: 1,
    recoil: 3.5,
    damage: 100, headshot: 100, range: 100,
  }
}

export function assaultrifleModel(tex, x=0, y=0, z=0) {
  const handle = createCube(.05/2, .2/2, -.075/2)
  transformCube(handle, 0, -.1/2, -.1/2, 30*Math.PI/180, 0, 0)

  const barrel = createCube(.1/2, .075/2, .75/2, handle.vertices.length/3)
  transformCube(barrel, 0, 0, .25/2)

  const aim = createCube(.05/2, .05/2, .08/2, handle.vertices.length/3 + barrel.vertices.length/3)
  transformCube(aim, 0, .045/2, .4/2, -15*Math.PI/180, 0, 0)

  const reload = createCube(.075/2, .25/2, -.1/2, handle.vertices.length/3 + barrel.vertices.length/3 + aim.vertices.length/3)
  transformCube(reload, 0, -.15/2, .3/2, 20*Math.PI/180, 0, 0)

  return {
    ...mixIntoModel([ handle, barrel, aim, reload ], tex, x, y, z),
    reload: .75,
    ammo: 10,
    firerate: .2,
    auto: true,
    scope: false,
    crosshair: 'crosshair.png',
    spread: 2,
    aim: 1,
    rounds: 1,
    recoil: 1,
    damage: 25, headshot: 75, range: 40,
  }
}

export function playerModel(gun, tex, x=0, y=0, z=0) {
  y -= .25

  const body = createCube(.2, .4, .1)
  const head = createCube(.3, .3, .3)
  transformCube(body, 0, -.05, 0)
  transformCube(head, 0, .25, 0)
  const bodyMod = mixIntoModel([ body ], tex, x, y, z)
  const headMod = mixIntoModel([ head ], tex, x, y, z)

  const leftArm = createCube(.05, .3, .05)
  const leftHand = createCube(.05, .3, .05)
  transformCube(leftArm, -.18, 0, -.04, .4, 0, -1)
  transformCube(leftHand, -.22, -.05, -.2, .8, 0, 2)
  const leftArmMod = mixIntoModel([ leftArm ], tex, x, y, z)
  const leftHandMod = mixIntoModel([ leftHand ], tex, x, y, z)

  const rightArm = createCube(.05, .3, .05)
  const rightHand = createCube(.05, .3, .05)
  transformCube(rightArm, .18, 0, -.04, .4, 0, 1)
  transformCube(rightHand, .22, -.05, -.2, .8, 0, -2)
  const rightArmMod = mixIntoModel([ rightArm ], tex, x, y, z)
  const rightHandMod = mixIntoModel([ rightHand ], tex, x, y, z)

  const leftLeg = createCube(.075, .2, .075)
  const leftFoot = createCube(.05, .2, .05)
  transformCube(leftLeg, -.05, -.35, 0)
  transformCube(leftFoot, -.05, -.55, 0)
  const leftLegMod = mixIntoModel([ leftLeg ], tex, x, y, z)
  const leftFootMod = mixIntoModel([ leftFoot ], tex, x, y, z)

  const rightLeg = createCube(.075, .2, .075)
  const rightFoot = createCube(.05, .2, .05)
  transformCube(rightLeg, .05, -.35, 0)
  transformCube(rightFoot, .05, -.55, 0)
  const rightLegMod = mixIntoModel([ rightLeg ], tex, x, y, z)
  const rightFootMod = mixIntoModel([ rightFoot ], tex, x, y, z)

  let time = 0
  const mixed = {
    d: [],
    o: [],

    position: [ x, y, z ],
    heading: 0, pitch: 0,
    walking: 0,

    player: true,
    update(delta) {
      let dpos = mixed.o[0].translation

      for (const o of mixed.o) {
        o.rotation[1] = lerpAngle(o.rotation[1], mixed.heading, 6 * delta)
        if (o == gun.o[0]) {
          const hcalc = Math.cos(o.rotation[1])
          const vcalc = Math.sin(o.rotation[1])
          const tx = mixed.position[0] + vcalc * .25
          const tz = mixed.position[2] + hcalc * .25

          o.translation[0] = lerp(o.translation[0], tx, 6 * delta)
          o.translation[1] = lerp(o.translation[1], mixed.position[1], 6 * delta)
          o.translation[2] = lerp(o.translation[2], tz, 6 * delta)
          continue
        }
        o.translation[0] = lerp(o.translation[0], mixed.position[0], 6 * delta)
        o.translation[1] = lerp(o.translation[1], mixed.position[1], 6 * delta)
        o.translation[2] = lerp(o.translation[2], mixed.position[2], 6 * delta)
      }

      headMod.o[0].rotation[0] = lerpAngle(headMod.o[0].rotation[0], mixed.pitch * .5, 6 * delta)

      const hcalc = Math.cos(mixed.o[0].rotation[1])
      const vcalc = Math.sin(mixed.o[0].rotation[1])

      gun.o[0].rotation[1] = mixed.o[0].rotation[1] + Math.PI

      if (mixed.walking > 0) {
        mixed.walking -= delta
        time += delta * 5
        if (time > 2*Math.PI)
          time -= 2*Math.PI
      } else {
        time = lerpAngle(time, 0, 5* delta)
      }
      leftLegMod.o[0].rotation[0] = Math.sin(time) * .3
      rightLegMod.o[0].rotation[0] = -Math.sin(time) * .3

      leftFootMod.o[0].translation[0]
        = dpos[0] - vcalc*.15*Math.sin(time)
      leftFootMod.o[0].translation[2]
        = dpos[2] - hcalc*.15*Math.sin(time)

      rightFootMod.o[0].translation[0]
        = dpos[0] + vcalc*.15*Math.sin(time)
      rightFootMod.o[0].translation[2]
        = dpos[2] + hcalc*.15*Math.sin(time)
    }
  }
  for (const { d, o } of [
    bodyMod, headMod,
    leftArmMod, leftHandMod,
    rightArmMod, rightHandMod,
    leftLegMod, leftFootMod,
    rightLegMod, rightFootMod,
    gun,
  ]) {
    mixed.d.push(...d)
    mixed.o.push(...o)
  }
  for (const obj of mixed.o) {
    obj.ref = mixed
    obj.scale = [.2, .5, .2]
  }

  return mixed
}
