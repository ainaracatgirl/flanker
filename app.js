if (!('randomUUID' in crypto)) {
  // https://stackoverflow.com/a/2117523
  crypto.randomUUID = function() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
  }
}

import { MockNetworking, WebSocketNetworking } from './net.js'
import { pickWeapon, setNetworking } from './util.js'

async function picked(pick) {
  document.querySelector('#weaponpick').close('1234')

  const id = new URLSearchParams(location.search).get('id')
  if (id) {
    setNetworking(await WebSocketNetworking.match(id))
  } else {
    setNetworking(await WebSocketNetworking.matchmaking())
  }

  pickWeapon(parseInt(pick || 0))
  document.querySelector('#display').requestPointerLock()
  import('./game.js')
}

document.querySelector('#weaponpick').addEventListener('close', ev => {
  if (ev.target.returnValue != '1234')
    document.querySelector('#weaponpick').showModal()
})
for (const btn of document.querySelectorAll('#weaponpick button')) {
  btn.addEventListener('click', async () => {
    await picked(btn.dataset.i)
  })
}

async function play() {
  document.querySelector('#weaponpick').showModal()
  const pick = new URLSearchParams(location.search).get('pick')
  if (pick) {
    await picked(pick)
  }
}

const rankq = {
  'Bronze I': 0,
  'Bronze II': 10,
  'Bronze III': 15,
  'Iron I': 20,
  'Iron II': 25,
  'Iron III': 35,
  'Silver I': 45,
  'Silver II': 50,
  'Silver III': 65,
  'Gold I': 70,
  'Gold II': 80,
  'Gold III': 90,
  'Platinum I': 100,
  'Platinum II': 125,
  'Platinum III': 150,
  'Top 1%': 200,
}
function loadStats() {
  const kval = localStorage.getItem('@flanker/kval') || 0
  let rnk = Object.keys(rankq)[0], nxtrnk = Object.keys(rankq)[1]
  for (const k in rankq) {
    if (rankq[k] <= kval) {
      nxtrnk = k
      rnk = k
    } else {
      nxtrnk = k
      break
    }
  }
  const val = rankq[rnk]
  const nxtval = rankq[nxtrnk]
  let prg = (kval-val) / (nxtval-val)
  if (rnk == nxtrnk)
    prg = 1
  document.querySelector('#info-rank').textContent = rnk
  document.querySelector('#info-progress span').style.width = `${(prg * 100).toFixed(1)}%`
}

const today = new Date()
if (localStorage.getItem('@flanker/rst') != `${today.getUTCMonth()}/${today.getUTCFullYear()}`) {
  localStorage.setItem('@flanker/kval', 0)
  localStorage.setItem('@flanker/rst', `${today.getUTCMonth()}/${today.getUTCFullYear()}`)
}
loadStats()

function timeLeftUntilNextMonth() {
  const today = new Date()
  const currentMonth = today.getMonth()
  const currentYear = today.getFullYear()

  const lastDayOfMonth = new Date(currentYear, currentMonth + 1, 0)
  const timeDifference = lastDayOfMonth - today

  const days = Math.floor(timeDifference / (1000 * 60 * 60 * 24))
  const hours = Math.floor((timeDifference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
  const minutes = Math.floor((timeDifference % (1000 * 60 * 60)) / (1000 * 60))
  const seconds = Math.floor((timeDifference % (1000 * 60)) / 1000)

  let fmt = ''
  if (days > 0)
    fmt += `${days}d `
  if (days > 0 || hours > 0)
    fmt += `${hours}h `
  if (days > 0 || hours > 0 || minutes > 0)
    fmt += `${minutes}min `
  fmt += `${seconds}s`


  document.querySelector('#info small').textContent = fmt
}
timeLeftUntilNextMonth()
setInterval(timeLeftUntilNextMonth, 1000)

document.querySelector('#info').showModal()
document.querySelector('#info').addEventListener('close', ev => {
  if (ev.target.returnValue != '1234')
    document.querySelector('#info').showModal()
})
document.querySelector('#info button').addEventListener('click', () => {
  document.querySelector('#info').close('1234')
  play()
})
